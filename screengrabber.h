#ifndef SCREENGRABBER_H
#define SCREENGRABBER_H

#include <QMainWindow>
#include <QList>
#include "mousetracker.h"
class QPixmap;
class QLabel;
class cMouseTracker;
class QPushButton;
class QLabel;
class QWidget;

class cScreenGrabber : public QMainWindow
{
    Q_OBJECT

public:
    explicit cScreenGrabber(QWidget *parent = 0);
    ~cScreenGrabber();
private slots:
    void mouseTrackReleased();
    void toggleScreenClicked();
private:
    void startMouseTracker();
    void shootScreen();
    void saveScreenShot();
    void closeEvent(QCloseEvent * event);
    QPixmap mScreenGrab;
    cMouseTracker * mMouseTracker;
    cScreenGrabCoOrds mCoOrds;
    QScreen * mScreen;
    QList<QScreen*> mScreenList;
    QWidget * mLayoutWidget;
    QPushButton * mToggleScreenButton;
    QLabel * mScreenNumber;
    int mCurrentScreenNo;

};

#endif // SCREENGRABBER_H
