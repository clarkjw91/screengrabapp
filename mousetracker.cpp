#include "mousetracker.h"

#include <iostream>
#include <QApplication>
#include <QDesktopWidget>
#include <QMouseEvent>
#include <QPoint>
#include <QRubberBand>
#include <QScreen>
cMouseTracker::cMouseTracker(int screenNumber, QWidget * parent): QWidget(parent)
{
   setWindowFlags(Qt::FramelessWindowHint);
   const QRect screenG = QApplication::desktop()->screenGeometry(screenNumber);

    setGeometry(screenG);
    setWindowOpacity(0.5);
    mouseDown = false;
    mSelection = new QRubberBand(QRubberBand::Rectangle, this);
    mSelection->setWindowOpacity(0.5);

}
cMouseTracker::~cMouseTracker(){
    delete mSelection;

}

void cMouseTracker::hideMouseTracking(){
    mSelection->hide();
    hide();
}
void cMouseTracker::resetMouseTracking(){
    mSelection->setGeometry(0,0,0,0);
    mSelection->show();
    show();
    mCoOrds = {0,0,0,0};
}
void cMouseTracker::mousePressEvent(QMouseEvent * event){
    mouseDown = true;
    QPoint p = event->pos();
    mCoOrds.mXPos = p.x();
    mCoOrds.mYPos = p.y();
    mSelection->setGeometry(mCoOrds.mXPos, mCoOrds.mYPos, 0 , 0);
    mSelection->show();
}

void cMouseTracker::mouseMoveEvent(QMouseEvent * event){
    if(mouseDown){
       QPoint p = event->pos();
       mSelection->resize(p.x() - mCoOrds.mXPos, p.y() - mCoOrds.mYPos);
    }
}
void cMouseTracker::mouseReleaseEvent(QMouseEvent * event){
    mouseDown = false;
    QPoint p = event->pos();
    mCoOrds.mWidth = p.x() - mCoOrds.mXPos;
    mCoOrds.mHeight = p.y() - mCoOrds.mYPos;
    emit mouseTrackRelease();
}
