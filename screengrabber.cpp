#include "screengrabber.h"
#include "mousetracker.h"

#include <QPixmap>
#include <QApplication>
#include <QDesktopWidget>
#include <QRect>
#include <QTimer>
#include <QScreen>
#include <QFileDialog>
#include <QList>
#include <QImageWriter>
#include <QStandardPaths>
#include <QMessageBox>
#include <QGuiApplication>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <iostream>



cScreenGrabber::cScreenGrabber(QWidget *parent) :
    QMainWindow(parent)
{
    mScreenList = QGuiApplication::screens();
    if(mScreenList.isEmpty()){
        std::cerr << "No Screen Found, Applications Quiting";
        close();
    }
    mCurrentScreenNo = QApplication::desktop()->screenNumber();
    mScreen = mScreenList[mCurrentScreenNo];
    setWindowFlag(Qt::WindowStaysOnTopHint);
    const QRect screenG = QApplication::desktop()->screenGeometry(this);
    setGeometry((screenG.width()/2)-(screenG.width()/16),
                (screenG.height()/2)-(screenG.height()/16),
                screenG.width()/8, screenG.height()/6);
    setAttribute(Qt::WA_QuitOnClose, false);
     mLayoutWidget = new QWidget(this);
     mLayoutWidget->setGeometry(0,0, this->width(), this->height());
    mLayoutWidget->show();
     mToggleScreenButton = new QPushButton("Toggle Screens", this);
     QLabel * ScreenNumberLabel = new QLabel;
     ScreenNumberLabel->setText("Current Screen:");
     mScreenNumber = new QLabel;
     mCurrentScreenNo = QApplication::desktop()->screenNumber(this);
     mScreenNumber->setText(QString::number(mCurrentScreenNo + 1));
     if(mScreenList.size() == 1)
         mToggleScreenButton->setEnabled(false);
     QString desc("Click and drag to make a screen grab selection, or use the toggle button to switch between screens.");
     QLabel * descLabel = new QLabel;
     descLabel->setText(desc);
     descLabel->setWordWrap(true);
     descLabel->setAlignment(Qt::AlignCenter);
     QHBoxLayout * screenLabelLayout = new QHBoxLayout;
     screenLabelLayout->addWidget(ScreenNumberLabel);
     screenLabelLayout->addWidget(mScreenNumber);
     QVBoxLayout * mainLayout = new QVBoxLayout;
     mainLayout->addWidget(mToggleScreenButton);
     mainLayout->addLayout(screenLabelLayout);
     mainLayout->addWidget(descLabel);
     mLayoutWidget->setLayout(mainLayout);

     startMouseTracker();
    connect(mToggleScreenButton, SIGNAL(clicked()), this, SLOT(toggleScreenClicked()));

}

cScreenGrabber::~cScreenGrabber()
{
    delete mMouseTracker;
    delete mScreen;
    delete mToggleScreenButton;
    delete mScreenNumber;

}
void cScreenGrabber::mouseTrackReleased(){
    mCoOrds = mMouseTracker->getCoOrds();
    mMouseTracker->hideMouseTracking();
    mLayoutWidget->hide();
    hide();
   QTimer::singleShot(500, this, &cScreenGrabber::shootScreen);
   QTimer::singleShot(500, this, &cScreenGrabber::saveScreenShot);
    show();
    mLayoutWidget->show();

}
void cScreenGrabber::toggleScreenClicked(){
    delete mMouseTracker;
    int maxScreens = mScreenList.size();
    if(mCurrentScreenNo == (maxScreens - 1)){
        mCurrentScreenNo = 0;
    }else{
        ++mCurrentScreenNo;
    }

    mScreen = mScreenList[mCurrentScreenNo];
    mScreenNumber->setText(QString::number(mCurrentScreenNo + 1));
    startMouseTracker();
}

void cScreenGrabber::startMouseTracker(){
    mMouseTracker = new cMouseTracker(mCurrentScreenNo);
    connect(mMouseTracker, SIGNAL(mouseTrackRelease()), this, SLOT(mouseTrackReleased()));
    mMouseTracker->show();
}
void cScreenGrabber::shootScreen(){
    if(mCoOrds.mHeight < 5 || mCoOrds.mWidth < 5)
        mScreenGrab = mScreen->grabWindow(0);
    else
        mScreenGrab = mScreen->grabWindow(0, mCoOrds.mXPos, mCoOrds.mYPos, mCoOrds.mWidth, mCoOrds.mHeight);
}

void cScreenGrabber::saveScreenShot(){
    const QString format = "png";
    QString initialPath = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    if( initialPath.isEmpty())
            initialPath = QDir::currentPath();
    initialPath += "/untitled." + format;

    QFileDialog fileDialog(this, "Save As", initialPath);
    fileDialog.setWindowFlag(Qt::WindowStaysOnTopHint);
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setFileMode(QFileDialog::AnyFile);
    fileDialog.setDirectory(initialPath);
    QList<QString> mimeTypes;
    foreach(const QByteArray &bf, QImageWriter::supportedMimeTypes())
        mimeTypes.append(QLatin1String(bf));
    fileDialog.setMimeTypeFilters(mimeTypes);
    fileDialog.selectMimeTypeFilter("image/" + format);
    fileDialog.setDefaultSuffix(format);
    if(fileDialog.exec() != QDialog::Accepted){
         mMouseTracker->resetMouseTracking();
        return;
     }
    const QString fileName = fileDialog.selectedFiles().first();
    if(!mScreenGrab.save(fileName)){
        QMessageBox::warning(this, tr("Save Error"), tr("The Image could not be saved %1").arg(QDir::toNativeSeparators(fileName)));
    }
    mMouseTracker->resetMouseTracking();
}

void cScreenGrabber::closeEvent(QCloseEvent * event){
    mMouseTracker->close();
    event->accept();
}
