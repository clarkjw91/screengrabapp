#ifndef MOUSETRACKER_H
#define MOUSETRACKER_H
#include <QWindow>
#include <QWidget>

class QMouseEvent;
class QRubberBand;

struct cScreenGrabCoOrds{
    int mXPos;
    int mYPos;
    int mWidth;
    int mHeight;
};

class cMouseTracker :public QWidget
{
    Q_OBJECT

public:
    explicit cMouseTracker(int screenNumber = 0, QWidget * parent = nullptr);
    ~cMouseTracker();
    cScreenGrabCoOrds getCoOrds() const {return mCoOrds;}
    void hideMouseTracking();
    void resetMouseTracking();
signals:
    void mouseTrackRelease();

private:
   void mousePressEvent(QMouseEvent *);
   void mouseMoveEvent(QMouseEvent *);
   void mouseReleaseEvent(QMouseEvent *);

   cScreenGrabCoOrds mCoOrds;
   QRubberBand * mSelection;
   bool mouseDown;

};

#endif // MOUSETRACKER_H
